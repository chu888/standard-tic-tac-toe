

def print_board(entries):
    line = "+---+---+---+"
    output = line
    n = 0
    for entry in entries:
# """ first condition"""
        if n % 3 == 0:
            output = output + "\n| "
        else:
            output = output + " | "
        output = output + str(entry)
# """" second condition"""
        if n % 3 == 2:
            output = output + " |\n"
            output = output + line

        n = n + 1
    print(output)



def game_over(current_playr, x):
    print_board(x)
    print('This is the winner ' + current_playr)
    exit()

def is_row_winner(board):
    if ((board[0] == board[1] and  board[1] == board[2]) or 
    (board[3] == board[4] and  board[4] == board[5]) or 
    (board[6] == board[7] and  board[7] == board[8])) :
        return True
    else:
        return False

def is_cross_winner(board):
    if((board[0] == board[4] and board[4] == board[8]) or
    (board[2] == board[4] and board[4] == board[6])):
        return True
    else: 
        return False

def is_column_winner(board):
    if((board[0] == board[3] and board[3] == board[6]) or
    (board[1] == board[4] and board[4] == board[7]) or
    (board[2] == board[5] and board[5] == board[8])):
        return True
    else:
        return False

mboard = [1, 2, 3, 4, 5, 6, 7, 8, 9]
 
    

current_player = "X"

for move_number in range(1, 10):
    print_board(mboard)
    response = input("Where would " + current_player + " like to move? ")
    space_number = int(response) - 1
    mboard[space_number] = current_player

    if is_row_winner(mboard):
        game_over(current_player, mboard)

    elif is_cross_winner(mboard):
       game_over(current_player, mboard)

    elif is_column_winner(mboard):
        game_over(current_player, mboard)

    if current_player == "X":
        current_player = "O"
    else:
        current_player = "X"

print("It's a tie!")
